from django.urls import path
from accounts.views import (
    UpdateStudentView,
    DeleteStudentView,
    StudentListView,
    FileDownloadView,
    GroupListView,
    CreateGroupView,
    UpdateGroupView,
    DeleteGroupView,
    TeachersListView,
    CreateTeacherView,
    UpdateTeacherView,
    DeleteTeacherView,
    ShowGroupsView,
)

app_name = "accounts"

urlpatterns = [
    path("", StudentListView.as_view(), name="get_students"),
    path("update/<str:pk>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<str:pk>/", DeleteStudentView.as_view(), name="delete_student"),
    path("download/<uuid:pk>/", FileDownloadView.as_view(), name="download_pdf_file"),
    path("get-groups/", GroupListView.as_view(), name="get_groups"),
    path("create-group/", CreateGroupView.as_view(), name="create_group"),
    path("update-group/<int:pk>/", UpdateGroupView.as_view(), name="update_group"),
    path("delete-group/<int:pk>", DeleteGroupView.as_view(), name="delete_group"),
    path("get-teachers", TeachersListView.as_view(), name="get_teachers"),
    path("create-teacher/", CreateTeacherView.as_view(), name="create_teacher"),
    path("update-teacher/<str:pk>/", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete-teacher/<str:pk>/", DeleteTeacherView.as_view(), name="delete_teacher"),
    path("show-groups/<str:pk>/", ShowGroupsView.as_view(), name="show_groups"),
]
