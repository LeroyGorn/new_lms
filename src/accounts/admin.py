from django.contrib import admin

from django.contrib.admin import ModelAdmin, StackedInline, TabularInline

from accounts.models import Profile, CustomUser, Group, GroupInfo  # NOQA

# Register your models here.


@admin.action(description="Set all students grades to 100 points")
def all_grade_to_max(models_admin, request, queryset):
    for user in queryset:
        user.profile.grade = 100
        user.profile.save()


@admin.action(description="SELECT ALL and Delete all students")
def delete_all_students(models_admin, request, queryset):
    queryset = CustomUser.objects.all()
    for user in queryset:
        if user.is_student:
            user.delete()


class ProfileAdminInline(StackedInline):
    model = Profile
    classes = ["collapse"]
    extra = 1


class GroupInfoAdminInline(TabularInline):
    model = GroupInfo
    extra = 1


@admin.register(CustomUser)
class CustomUserAdmin(ModelAdmin):
    inlines = [ProfileAdminInline, GroupInfoAdminInline]
    actions = [all_grade_to_max, delete_all_students]
    ordering = ("email",)
    search_fields = (
        "email",
        "phone_number",
        "username__icontains",
    )

    list_display = (
        "username",
        "phone_number",
        "email",
        "is_staff",
        "is_teacher",
        "is_mentor",
        "is_student",
        "position",
        "date_joined",
    )

    fieldsets = (
        (None, {"fields": ("username", "email", "phone_number")}),
        (
            "Permissions",
            {
                "classes": ("wide",),
                "fields": (
                    ("is_staff", "is_active"),
                    ("is_student", "is_teacher", "is_mentor"),
                ),
            },
        ),
    )


class ProfileAdmin(ModelAdmin):

    list_display = (
        "user",
        "full_name",
        "age",
        "job_title",
        "grade",
    )
    search_fields = (
        "first_name__icontains",
        "last_name__icontains",
        "job_title__icontains",
    )


@admin.register(Group)
class GroupAdmin(ModelAdmin):
    inlines = [GroupInfoAdminInline]
