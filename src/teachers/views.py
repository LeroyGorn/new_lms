from django.forms.utils import ErrorList
from django.urls import reverse_lazy

from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView, ListView

from students.models import CustomUser
from students.views import UserAccessMixin


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ["get"]


class TeachersListView(ListView):
    model = CustomUser
    context_object_name = "teachers"
    template_name = "teachers_list.html"
    queryset = CustomUser.objects.filter(is_teacher=True)


class CreateTeacherView(UserAccessMixin, CreateView):
    permission_required = "teachers.change_teacher"
    login_url = "student_login"
    redirect_field_name = "next"

    template_name = "teachers_create.html"
    model = CustomUser
    fields = "__all__"
    success_url = reverse_lazy("teachers:get_teachers")
    initial = {"first_name": "John", "last_name": "Doe"}

    def form_valid(self, form):
        self.object = form.save(commit=False)
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        if first_name == last_name:
            form._errors["first_name"] = ErrorList(["First and last names can't be equal"])
            form._errors["last_name"] = ErrorList(["First and last names can't be equal"])
            return super().form_invalid(form)
        return super().form_valid(form)


class UpdateTeacherView(UserAccessMixin, UpdateView):
    permission_required = "teachers.change_teacher"
    login_url = "student_login"
    redirect_field_name = "next"

    model = CustomUser
    template_name = "edit_teacher.html"
    fields = "__all__"
    success_url = reverse_lazy("teachers:get_teachers")
    http_method_names = ["get", "post"]


class DeleteTeacherView(UserAccessMixin, DeleteView):
    permission_required = "teachers.change_teacher"
    login_url = "student_login"
    redirect_field_name = "next"

    model = CustomUser
    success_url = reverse_lazy("teachers:get_teachers")
    template_name = "delete_confirm.html"


class ShowGroupsView(DetailView):
    model = CustomUser
    template_name = "show_groups.html"


class NotFoundView(TemplateView):
    template_name = "404.html"
