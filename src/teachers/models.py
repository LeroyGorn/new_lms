# import datetime
# import random
# import uuid
#
# from django.db import models
# from faker import Faker
#
#
# class Teacher(models.Model):
#     id = models.UUIDField(
#         primary_key=True,
#         unique=True,
#         editable=False,
#         default=uuid.uuid4,
#     )
#     first_name = models.CharField(max_length=128, null=True)
#     last_name = models.CharField(max_length=128, null=True)
#     email = models.EmailField(max_length=128)
#     birthday = models.DateField(null=True)
#     experience = models.SmallIntegerField(null=True)
#     salary = models.SmallIntegerField(null=True)
#     group = models.ManyToManyField(to="groups.Group", related_name="teachers")
#
#     def __str__(self):
#         return f"{self.first_name} {self.last_name} {self.email} {self.id}"
#
#     def age(self):
#         return datetime.datetime.now().year - self.birthday.year
#
#     def generate_teacher(self, count):
#         faker = Faker()
#
#         for _ in range(count):
#             salary = 2500 + random.randint(200, 500)
#             experience = random.randint(5, 15)
#             if 10 < experience < 30:
#                 salary += random.randrange(4000, 5000, 500)
#             elif 5 < experience <= 10:
#                 salary += random.randrange(3000, 4000, 200)
#             Teacher.objects.create(
#                 first_name=faker.first_name(),
#                 last_name=faker.last_name(),
#                 email=faker.email(),
#                 birthday=faker.date_time_between(start_date="-80y", end_date="-35y"),
#                 experience=experience,
#                 salary=salary,
#             )
