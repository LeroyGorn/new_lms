from django.urls import path
from teachers.views import CreateTeacherView, UpdateTeacherView, DeleteTeacherView, ShowGroupsView, TeachersListView

app_name = "teachers"


urlpatterns = [
    path("", TeachersListView.as_view(), name="get_teachers"),
    path("create/", CreateTeacherView.as_view(), name="create_teacher"),
    path("update/<str:pk>/", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete/<str:pk>/", DeleteTeacherView.as_view(), name="delete_teacher"),
    path("groups/<str:pk>/", ShowGroupsView.as_view(), name="show_groups"),
]
