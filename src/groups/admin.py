from django.contrib import admin  # NOQA
from groups.models import Group

# Register your models here.
admin.site.register(Group)
