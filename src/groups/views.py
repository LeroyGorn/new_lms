from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from groups.forms import GroupForm
from groups.models import Group
from students.views import UserAccessMixin


class GroupListView(ListView):
    model = Group
    context_object_name = "groups"
    template_name = "groups_list.html"
    queryset = Group.objects.all()


class CreateGroupView(UserAccessMixin, CreateView):
    permission_required = "students.change_student"
    login_url = "groups:get_groups"
    redirect_field_name = "next"

    template_name = "create_group.html"
    model = Group
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_groups")


class UpdateGroupView(UserAccessMixin, UpdateView):
    permission_required = "students.change_student"
    login_url = "groups:get_groups"
    redirect_field_name = "next"

    model = Group
    template_name = "edit_teacher.html"
    success_url = reverse_lazy("groups:get_groups")
    form_class = GroupForm


class DeleteGroupView(UserAccessMixin, DeleteView):
    model = Group
    success_url = reverse_lazy("groups:get_groups")
    template_name = "delete_confirm_group.html"
