import random
from itertools import product

from django.db import models
from faker import Faker

fake = Faker()


class Group(models.Model):
    department = models.CharField(max_length=128, null=True)
    course = models.SmallIntegerField(null=True)
    letter = models.CharField(max_length=128, null=True)
    classroom = models.SmallIntegerField(null=True, unique=True)
    max_students = models.SmallIntegerField(null=True)

    def generate_groups(self, count):
        department = [
            "Math",
            "Physics",
            "Biology",
        ]
        group_name = [
            "A",
            "B",
            "C",
        ]
        course_number = range(1, 6)
        random_data = list(
            product(
                department,
                group_name,
                course_number,
            )
        )
        random.shuffle(random_data)
        n = 0
        for group in random_data:
            n += 1
            Group.objects.create(
                department=group[0], letter=group[1], course=group[2], classroom=random.randint(1, 400), max_students=15
            )
            if n == count:
                break

    def __str__(self):
        return (
            f"Department: {self.department} "
            f"Course: {self.course} "
            f"Letter: {self.letter} "
            f"Classroom: {self.classroom} "
            f"Id: {self.id}"
        )
