from django.urls import path
from students.views import (
    UpdateStudentView,
    DeleteStudentView,
    StudentListView,
    FileDownloadView,
)

app_name = "students"
urlpatterns = [
    path("", StudentListView.as_view(), name="get_students"),
    path("update/<str:pk>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<str:pk>/", DeleteStudentView.as_view(), name="delete_student"),
    path("download/<uuid:pk>/", FileDownloadView.as_view(), name="download_pdf_file"),
]
