import datetime
import uuid

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from students.managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    id = models.UUIDField(
        primary_key=True,
        unique=True,
        editable=False,
        default=uuid.uuid4,
    )
    email = models.EmailField(_("email address"), unique=True)
    username = models.CharField(_("username"), max_length=35, unique=True)
    phone_number = PhoneNumberField(_("phone number"), unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_student = models.BooleanField(default=False, verbose_name="Student")
    is_teacher = models.BooleanField(default=False, verbose_name="Teacher")
    is_mentor = models.BooleanField(default=False, verbose_name="Mentor")
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = ["username", "email"]
    objects = CustomUserManager()

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    @property
    def position(self):
        if self.is_student:
            position = "Student"
            self.is_mentor = False
            self.is_teacher = False
            return position
        elif self.is_teacher:
            position = "Teacher"
            if self.is_mentor:
                position = "Teacher and Mentor"
            return position
        elif self.is_mentor and not self.is_teacher:
            position = "Mentor"
            return position
        if self.is_staff:
            position = "Admin"
            return position

    def __str__(self):
        return f"{self.email} {self.position}"


class ProxyUser(get_user_model()):
    class Meta:
        proxy = True
        ordering = ("-username",)


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    birthdate = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to="static/img/user_profile/", blank=True, default="images/student/default.png")
    # phone_number = models.CharField(max_length=16, blank=True, null=False)
    first_name = models.CharField(_("name"), max_length=150, blank=True)
    last_name = models.CharField(_("surname"), max_length=150, blank=True)
    job_title = models.CharField(blank=True, max_length=150)
    grade = models.PositiveSmallIntegerField(
        blank=True, null=False, default=1, validators=[MaxValueValidator(100), MinValueValidator(1)]
    )

    class Meta:
        verbose_name = _("profile")
        verbose_name_plural = _("profiles")

    def age(self):
        return datetime.datetime.now().year - self.birthdate.year

    @property
    def full_name(self):
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    @receiver(post_save, sender=get_user_model())
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=get_user_model())
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        return f"Profile for {self.user.username}, {self.full_name}"
