from django.contrib import admin

# from django.contrib.auth.admin import UserAdmin
# from django.contrib.auth.models import Permission
#
# from students.forms import CustomUserChangeForm, CustomUserCreationForm
from django.contrib.auth import get_user_model

from students.models import Profile  # NOQA

# Register your models here.

admin.site.register(Profile)
admin.site.register(get_user_model())


# class CustomUserAdmin(UserAdmin):
#     add_form = CustomUserCreationForm
#     form = CustomUserChangeForm
#     model = CustomUser
#     list_display = (
#         "email",
#         "is_staff",
#         "is_active",
#         "is_student",
#         "is_teacher",
#         "is_mentor",
#     )
#     list_filter = (
#         "email",
#         "is_staff",
#         "is_active",
#         "is_student",
#         "is_teacher",
#         "is_mentor",
#     )
#     fieldsets = (
#         (None, {"fields": ("username", "first_name", "last_name", "sex")}),
#         (
#             "Permissions",
#             {
#                 "fields": (
#                     "is_staff",
#                     "is_active",
#                     "is_student",
#                     "is_teacher",
#                 )
#             },
#         ),
#     )
#     add_fieldsets = (
#         (
#             None,
#             {
#                 "classes": ("wide",),
#                 "fields": (
#                     "username",
#                     "position",
#                     "sex",
#                     "photo",
#                     "email",
#                     "birthdate",
#                     "password1",
#                     "password2",
#                 ),
#             },
#         ),
#     )
#     search_fields = ("email",)
#     ordering = ("email",)
