from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.core.exceptions import ValidationError

from django.forms import ModelForm, EmailField, CharField, PasswordInput
from phonenumber_field.formfields import PhoneNumberField


from students.models import CustomUser, Profile
from django.utils.translation import gettext_lazy as _


class CustomUserCreationForm(UserCreationForm):
    phone_number = PhoneNumberField()
    email = EmailField(max_length=200, help_text="Please use your working email")

    class Meta:
        model = CustomUser
        fields = (
            "phone_number",
            "email",
            "username",
            "is_student",
            "is_teacher",
            "is_mentor",
            "password1",
            "password2",
        )

    def clean(self):
        cleaned_data = super().clean()
        # extend parent method,  not overwrite

        student = self.cleaned_data.get("is_student")
        teacher = self.cleaned_data.get("is_teacher")
        mentor = self.cleaned_data.get("is_mentor")

        if student:
            if teacher or mentor:
                raise ValidationError("Student cannot be a teacher or mentor")
        return cleaned_data

    def clean_phone_number(self):
        phone_number = self.cleaned_data.get("phone_number")
        phone_number_qs = CustomUser.objects.filter(phone_number=phone_number)
        if phone_number_qs.exists():
            raise ValidationError("Phone number already exists")
        return phone_number

    def clean_username(self):
        username = self.cleaned_data.get("username")
        username_qs = CustomUser.objects.filter(username=username)
        if username_qs.exists():
            raise ValidationError("Username already exists")
        return username

    def clean_email(self):
        email = self.cleaned_data.get("email")
        email_qs = CustomUser.objects.filter(email=email)
        if email_qs.exists():
            raise ValidationError("This email has been already in use")
        return email


class CustomUserChangeForm(ModelForm):
    class Meta:
        model = CustomUser
        fields = (
            "email",
            "username",
            "phone_number",
            "is_student",
            "is_teacher",
            "is_mentor",
        )


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = (
            "birthdate",
            "photo",
            "first_name",
            "last_name",
            "job_title",
        )


class CustomLoginForm(AuthenticationForm):
    password = CharField(
        label=_("Password"),
        strip=False,
        widget=PasswordInput(attrs={"autocomplete": "current-password"}),
    )
