import os

from django.contrib.auth import login
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Permission
from django.contrib.auth.views import LoginView, LogoutView, redirect_to_login
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views import View

from django.views.generic import UpdateView, DeleteView, CreateView, ListView, RedirectView, DetailView, TemplateView


from students.forms import CustomUserCreationForm, CustomUserChangeForm, CustomLoginForm, ProfileForm
from students.models import CustomUser, Profile
from students.services.emails import send_registration_email
from students.utils.token_generator import TokenGenerator


class UserAccessMixin(PermissionRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
        if not self.has_permission():
            return redirect("login")
        return super(UserAccessMixin, self).dispatch(request, *args, **kwargs)


class StudentListView(ListView):
    model = CustomUser
    context_object_name = "students"
    template_name = "students_list.html"
    queryset = CustomUser.objects.filter(is_student=True)


class UpdateStudentView(UserAccessMixin, UpdateView):
    permission_required = "students.delete_student"
    login_url = "login"
    redirect_field_name = "next"

    model = CustomUser
    template_name = "edit_student.html"
    success_url = reverse_lazy("students:get_students")
    form_class = CustomUserChangeForm


class DeleteStudentView(UserAccessMixin, DeleteView):
    permission_required = "students.delete_student"
    login_url = "login"
    redirect_field_name = "next"

    model = CustomUser
    success_url = reverse_lazy("students:get_students")
    template_name = "delete_confirm_student.html"


class FileDownloadView(UserAccessMixin, View):
    permission_required = "students.change_student"
    login_url = "login"
    redirect_field_name = "next"

    model = CustomUser
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    folder_path = BASE_DIR + "/media/images/student/cv/"
    filename = ""
    content_type_value = "application/pdf"

    def get(self, request, pk):
        student = get_object_or_404(CustomUser, pk=pk)
        filename = student.filename
        file_path = os.path.join(self.folder_path, filename)
        if os.path.exists(file_path):
            with open(file_path, "rb") as fh:
                response = HttpResponse(fh.read(), content_type=self.content_type_value)
                response["Content-Disposition"] = "attachment; filename=" + os.path.basename(file_path)
            return response
        else:
            raise Http404


class UserLoginView(LoginView):
    form_class = CustomLoginForm
    template_name = "registration/login.html"

    # def get_success_url(self):
    #     return reverse("profile", args=[self.request.user.username]) or self.get_default_redirect_url()

    def get_default_redirect_url(self):
        return reverse("index")


class UserLogOutView(LogoutView):
    next_page = reverse_lazy("login")


class UserRegistrationView(CreateView):
    template_name = "registration/registration.html"
    form_class = CustomUserCreationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(request=self.request, user_instance=self.object)
        return super().form_valid(form)


class ActivateUserView(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, backend="django.contrib.auth.backends.ModelBackend", *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = CustomUser.objects.get(pk=pk)
        except (CustomUser.DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data")

        if current_user and TokenGenerator().check_token(current_user, token):
            perm1 = Permission.objects.get(codename="change_profile")
            perm2 = Permission.objects.get(codename="view_profile")
            current_user.is_active = True
            current_user.user_permissions.add(perm1, perm2)
            current_user.backend = "django.contrib.auth.backends.ModelBackend"
            current_user.save()

            login(request, current_user)
            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data")


class CustomUserUpdateView(UserAccessMixin, UpdateView):
    permission_required = "students.change_customuser"
    model = CustomUser
    form_class = CustomUserChangeForm
    success_url = reverse_lazy("index")
    template_name = "update_customuser.html"


class CustomUserProfileView(UserAccessMixin, DetailView):
    model = Profile
    permission_required = "students.view_profile"
    template_name = "view_user_profile.html"

    def get_object(self):
        return get_object_or_404(Profile, user__username=self.kwargs["username"])


class CustomUserProfileUpdateView(UserAccessMixin, UpdateView):
    permission_required = "students.change_profile"
    login_url = "login"
    redirect_field_name = "next"
    model = Profile
    template_name = "edit_user_profile.html"
    success_url = reverse_lazy("index")
    form_class = ProfileForm

    def get_object(self):
        return get_object_or_404(Profile, user__id=self.kwargs["pk"])


class PrivacyPolicyView(TemplateView):
    template_name = "privacy_policy.html"
