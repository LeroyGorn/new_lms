asgiref==3.5.2
asttokens==2.0.5
backcall==0.2.0
beautifulsoup4==4.10.0
black==22.3.0
certifi==2022.5.18.1
cffi==1.15.0
charset-normalizer==2.0.12
click==8.1.3
crispy-bootstrap5==0.6
cryptography==37.0.2
cssutils==2.4.0
decorator==5.1.1
defusedxml==0.7.1
Django==3.2.14
django-admin-interface==0.19.1
django-colorfield==0.7.0
django-crispy-forms==1.14.0
django-debug-toolbar==3.4.0
django-extensions==3.1.5
django-flat-responsive==2.0
django-flat-theme==1.1.4
django-inlinecss-redux==0.4.0
django-phone-auth==0.3.1
django-phonenumber-field==6.1.0
executing==0.8.3
Faker==13.12.0
flake8==4.0.1
idna==3.3
importlib-metadata==4.2.0
ipython==7.34.0
jedi==0.18.1
marshmallow==3.16.0
matplotlib-inline==0.1.3
mccabe==0.6.1
mypy-extensions==0.4.3
oauthlib==3.2.0
packaging==21.3
parso==0.8.3
pathspec==0.9.0
pexpect==4.8.0
phonenumbers==8.12.49
pickleshare==0.7.5
Pillow==9.1.1
platformdirs==2.5.2
prompt-toolkit==3.0.29
ptyprocess==0.7.0
pure-eval==0.2.2
pycodestyle==2.8.0
pycparser==2.21
pyflakes==2.4.0
Pygments==2.12.0
PyJWT==2.4.0
pynliner==0.8.0
pyparsing==3.0.9
python-dateutil==2.8.2
python3-openid==3.2.0
pytz==2022.1
requests==2.27.1
requests-oauthlib==1.3.1
six==1.16.0
social-auth-app-django==5.0.0
social-auth-core==4.2.0
soupsieve==2.3.2
sqlparse==0.4.2
stack-data==0.2.0
tomli==2.0.1
traitlets==5.2.2.post1
typed-ast==1.5.4
typing_extensions==4.2.0
urllib3==1.26.9
wcwidth==0.2.5
webargs==8.1.0
zipp==3.8.0
